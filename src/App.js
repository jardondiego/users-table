import React, { Component } from 'react';
import UsersTable from './components/UsersTable/UsersTable';
import {normalizeObject} from './utilities';
import axios from 'axios';
import './App.css';

class App extends Component {
  constructor (props) {
    super(props);

    this.state = {
      users: [
        {}
      ]
    };

  }

  componentDidMount() {
    this.fetchUsers();
  }

  fetchUsers () {
    return axios.get('https://jsonplaceholder.typicode.com/users/').then(res => {
      let purgeAddresses = res.data.map(user => {
        delete user.address.geo;
        return user;
      });

      let normalizedObjects = res.data.map(user => normalizeObject(user, ' '));
      this.setState({users: normalizedObjects});
    });
  }

  render () {
    return (
        
        <UsersTable users={this.state.users} fields={["name", "username", "address", "website"]}/>
    );
  }
}

export default App;
